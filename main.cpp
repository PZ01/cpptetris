#include "GameLoop.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
LOG:
[ver 1.0]
KNOWN BUGS / Limitations:
Rotation of blocks at high speed will merge them with already landed blocks.
Spacebar triggers instant drop on next spawned block when holding for slithly too long.
No game-over overlay when loosing a game.
No pause.
*/
int main()
{
	srand (time(NULL));
	GameLoop loop;
	loop.start();

	return 0;
}
