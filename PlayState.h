/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/

#pragma once
#include "GameState.h"
#include "GameGrid.h"
#include <SFML/Audio.hpp>

class Shape;
class ScoreBoard;

/**
This class handles all of the game logic for the Blockz game :
-Block movement
-Input
-Score
**/
class PlayState : public GameState
{
public:
	PlayState(int pWindowWidth, int pWindowHeight);
	int handleEvents(sf::RenderWindow *);
	void logic();
	void draw(sf::RenderWindow *);
	~PlayState(void);

private:
	//Graphics
	int _windowWidth;
	int _windowHeight;

	//Game logic
	const int NUM_OF_DIFF_BLOCKS;
	bool _gameOver;
    double _movedownFrequency;

	GameGrid _gameGrid;
    ScoreBoard *_scoreBoard;
	Shape *_droppingShape;
	sf::Clock _gameClock;
	sf::Clock _keyPressClock;
	sf::Sound _sound;

	Constants::GameShapes generateShapeID();
	int generateHorizSpawnPos();
	void increaseSpeed();
};

