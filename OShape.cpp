#include "OShape.h"
#include <iostream>
#include "Constants.h"
#include "RM.h"

const static int SHAPE_WIDTH = 2;
const static int SHAPE_HEIGHT = 2;

OShape::OShape(Constants::GameShapes pShapeType, int pPosX) :
	Shape(pShapeType, SHAPE_WIDTH, SHAPE_HEIGHT, pPosX)
{
	_shapeGrid[0][0] = Constants::O;
	_shapeGrid[0][1] = Constants::O;
	_shapeGrid[1][0] = Constants::O;
	_shapeGrid[1][1] = Constants::O;
}

OShape::~OShape(void)
{
}
