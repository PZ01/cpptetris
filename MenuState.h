/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/

#pragma once
#include "gamestate.h"
#include "Menu.h"

class MenuState : public GameState
{
public:
	MenuState(int pWindowWidth, int pWindowHeight);
	int handleEvents(sf::RenderWindow *);
	void logic();
	void draw(sf::RenderWindow *);
	~MenuState(void);

private:
	Menu _menu;
	sf::Sprite _menuBG;
};

