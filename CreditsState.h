/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/

#pragma once
#include "gamestate.h"

class Clickable;

/**
This state represents the Credits section.
**/
class CreditsState : public GameState
{
public:
	int handleEvents(sf::RenderWindow *);
	void logic();
	void draw(sf::RenderWindow *);
	CreditsState(int pWindowWidth, int pWindowHeight);
	~CreditsState(void);

private:
    sf::Text _text;
    static const std::string CREDITS_TEXT1;
    static const std::string CREDITS_TEXT2;
    static const std::string CREDITS_TEXT3;
    static const std::string CREDITS_TEXT4;

    Clickable *_menuMenuItem;
    sf::Clock _splashTimeClock;
    static int _entriesCnt;

    //Array holding text entries.
    const int NUM_ENTRIES = 4;
    const int DELAY_ENTRIES_SECONDS = 3;
    std::string *_textFieldsArr;
    bool _isDone;
};

