/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/
#pragma once

#include "SFML/Graphics/Color.hpp"

/**
A bunch of constants used troughout the other classes.
**/
class Constants
{
public:
	Constants(void);
	~Constants(void);

	//Game states
	enum GameStates
	{
		STATE_NULL,
		STATE_SPLASH,
		STATE_MENU,
		STATE_GAME,
		STATE_CREDITS,
		STATE_EXIT
	};

	enum GameShapes
	{//Reserving zero(0) for empty tile
		I = 1,
		J = 2,
		L = 3,
		O = 4,
		S = 5,
		T = 6,
		Z = 7
	};

	enum TextureType
	{
		UPTODOWN,
		LEFTTORIGHT,
		CLOCKWISE
	};

	enum Buttons
	{
		MENU_START,
		MENU_CREDITS,
		MENU_QUIT,
		MENU_MENU
	};

	enum PossibleMove
	{
		MOVE_LEFT = -1,
		MOVE_RIGHT = 1,
		MOVE_DOWN = 0,
		MOVE_UP = 2
	};

	//Constants
	static const int GRID_BORDER_WIDTH = 40;
	static const int GRID_WIDTH = 20;
	static const int GRID_HEIGHT = 20;

	static const int BG_MENU_POSX = -400;
	static const int BG_MENU_POSY = -400;

	static const int FONT_SIZE = 50;

	static const int SCORE_TILE_DROP = 10;
	static const int SCORE_TILE_CLEAR = 100;
	static const int SCORE_TILE_BONUS = 150;
	static const int SCORE_LEVEL_UP = 250;
};

