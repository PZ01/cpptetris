/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/

#pragma once
#include <SFML/Graphics.hpp>

class GameState
{
public:
	GameState(void);

	virtual int handleEvents(sf::RenderWindow *) = 0;
	virtual void logic() = 0;
	virtual void draw(sf::RenderWindow *) = 0;
	~GameState(void);
};

