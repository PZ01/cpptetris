#include "PlayState.h"
#include "Shape.h"
#include "ShapeFactory.h"
#include <iostream>
#include "Constants.h"
#include "RM.h"
#include "ScoreBoard.h"

PlayState::PlayState(int pWindowWidth, int pWindowHeight) :
	_windowWidth(pWindowWidth),
	_windowHeight(pWindowHeight),
	_droppingShape(NULL),
	NUM_OF_DIFF_BLOCKS(7),
	_gameGrid(Constants::GRID_WIDTH,Constants::GRID_HEIGHT, pWindowWidth, pWindowHeight),
	_gameOver(false),
	_movedownFrequency(0.5)
{
    sf::Sprite board(RM::getInstance()->boardTexture());
    int leftOfGridPosition = (float(pWindowWidth) / 2) - ((board.getGlobalBounds().width) / 2);
	int topOfGridPosition = (float(pWindowHeight) / 2) - ((board.getGlobalBounds().height) / 2);

	_scoreBoard = new ScoreBoard(leftOfGridPosition, topOfGridPosition);
}

int PlayState::handleEvents(sf::RenderWindow *pWindow)
{
	using namespace sf;

	Event event;

	//Handle infrequent events
	while (pWindow->pollEvent(event))
	{
		if(event.type == sf::Event::MouseButtonPressed)
		{
			int mouseX = event.mouseButton.x;
			int mouseY = event.mouseButton.y;

			return _scoreBoard->clickedQuit(mouseX,mouseY);
		}
		else if(event.type == sf::Event::MouseMoved)
		{
			int mouseX = event.mouseMove.x;
			int mouseY = event.mouseMove.y;

			_scoreBoard->clickedQuit(mouseX,mouseY);
		}

	}

	if (_gameOver)
	{
		return Constants::STATE_MENU;
	}

	//Don't handle these events if no block is currently falling.
	if(_droppingShape != NULL && _keyPressClock.getElapsedTime().asMilliseconds() > 100)
	{
		bool keyTriggered = false;

		//Handle frequent events
		if (Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			_gameGrid.attempteMoveShapeByKeys(_droppingShape, Constants::MOVE_LEFT);
			keyTriggered = true;
		}
		else if (Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			_gameGrid.attempteMoveShapeByKeys(_droppingShape, Constants::MOVE_RIGHT);
			keyTriggered = true;
		}
		else if (Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			_gameGrid.attempteMoveShapeByKeys(_droppingShape, Constants::MOVE_DOWN);
			keyTriggered = true;
		}
		else if (Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			_gameGrid.attemptRotateShape(_droppingShape);
			keyTriggered = true;
		}
		else if (Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			_gameGrid.attemptInstantDrop(_droppingShape);
			keyTriggered = true;
		}

		if (keyTriggered)
		{
			_keyPressClock.restart();
		}
	}
	return Constants::STATE_NULL;
}

void PlayState::logic()
{
	if(_droppingShape == NULL) //No shape in the air, spawn one.
	{
		_droppingShape = ShapeFactory::getInstance().createShape(generateShapeID(), generateHorizSpawnPos());
		_gameClock.restart();
	}
	else
	{
		if(_gameClock.getElapsedTime().asSeconds() > _movedownFrequency)
		{
			bool canMoveDown = _gameGrid.attemptPeriodicShapeMoveDown(_droppingShape);

			if(!canMoveDown)
			{
				//Once we put the shape down we,
				//-Delete the falling shape since it's imprinted in the grid.
				//-We possibly clear filled rows by the player.
				//-We check for a loss.
				//-Sound on hit.
				//Deal with scoring logic
				delete _droppingShape;
				_droppingShape = NULL;

				_sound.setBuffer(RM::getInstance()->hitSoundBuffer());
				_sound.play();

				_scoreBoard->blockDropped();

                int numRowsCleared = 0;
				bool rowsWereCleared = _gameGrid.possiblyClearRows(numRowsCleared);
				if (rowsWereCleared)
				{
					_sound.setBuffer(RM::getInstance()->clearSoundBuffer());
					_sound.play();

					_scoreBoard->clearedRows(numRowsCleared);
				}

				_scoreBoard->checkScore();
				increaseSpeed();
				_gameOver = _gameGrid.checkForLoss();
			}

			_gameClock.restart();
		}
	}
}

void PlayState::increaseSpeed()
{
    int currLevel = _scoreBoard->getCurrLevel();

    switch( currLevel )
    {
		case 2:
			_movedownFrequency = 0.3;
			break;
        case 3:
			_movedownFrequency = 0.2;
			break;
        case 4:
            _movedownFrequency = 0.1;
			break;
        case 5:
            _movedownFrequency = 0.0;
            break;
    }
}

void PlayState::draw(sf::RenderWindow *pWindow)
{
	//Draws the board and the LANDED blocks
	_gameGrid.draw(pWindow);

    //Draws the score board
    _scoreBoard->draw(pWindow);

	//Draws the floating block
	if(_droppingShape != NULL)
	{
		_droppingShape->draw(pWindow, _gameGrid.getLeftOfGridPosition(), _gameGrid.getTopOfGridPosition());
	}
}

Constants::GameShapes PlayState::generateShapeID()
{
	return (Constants::GameShapes) (rand() % 7);
}

int PlayState::generateHorizSpawnPos()
{
	return rand() % 18;
}

PlayState::~PlayState(void)
{
}
