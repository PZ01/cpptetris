# README #

This quick experiment evaluated SFML as a potential library for sprite rendering and interactivity. To play around with the API I wrote a simple Tetris game.

### What is this repository for? ###

* A Tetris game written in C++ that was the target of an android port.

### How do I get set up? ###

* Install Codeblocks, open the project file and compile!
