/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/

#pragma once
#include <SFML/Graphics.hpp>

class GameState;

/**
This class is the main execution loop for the game, it calls out virtual members from the State base class
and takes care of instanciating the proper states when required.
**/
class GameLoop
{
public:
	GameLoop(); //TODO create properties file
	void setNextState(int state);
	void changeState();
	~GameLoop(void);
	void start();

private:
	GameState *_gameState;
	sf::RenderWindow _window;
	int _currentStateID;
	int _nextStateID;
};

