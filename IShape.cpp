#include "IShape.h"


IShape::IShape(Constants::GameShapes pShapeType, int pPosX) :
	Shape(pShapeType, 1, 4, pPosX)
{
	_shapeGrid[0][0] = Constants::I;
	_shapeGrid[1][0] = Constants::I;
	_shapeGrid[2][0] = Constants::I;
	_shapeGrid[3][0] = Constants::I;
}

void IShape::potentialRotate()
{
	if(_rotationCycle == 0)
	{
		clearGrid(_potentialShapeGrid);
		resize2Dimension(_potentialShapeGrid, 1, 4);
		//|I|I|I|I|
		_potentialShapeGrid[0][0] = Constants::I;
		_potentialShapeGrid[0][1] = Constants::I;
		_potentialShapeGrid[0][2] = Constants::I;
		_potentialShapeGrid[0][3] = Constants::I;
	}
	else if(_rotationCycle == 1)
	{
		clearGrid(_potentialShapeGrid);
		resize2Dimension(_potentialShapeGrid, 4, 1);
		//|I|
		//|I|
		//|I|
		//|I|
		_potentialShapeGrid[0][0] = Constants::I;
		_potentialShapeGrid[1][0] = Constants::I;
		_potentialShapeGrid[2][0] = Constants::I;
		_potentialShapeGrid[3][0] = Constants::I;
	}


	if(_rotationCycle++ == 1)
		_rotationCycle = 0;
}

void IShape::rotate()
{
	_shapeGrid = _potentialShapeGrid;
}

IShape::~IShape(void)
{
}
