#include "JShape.h"
#include "Constants.h"
#include "RM.h"

JShape::JShape(Constants::GameShapes pShapeType, int pPosX) :
	Shape(pShapeType, 2, 3, pPosX)//rename width height to cols...
{
	_shapeGrid[0][1] = Constants::J;
	_shapeGrid[1][1] = Constants::J;
	_shapeGrid[2][0] = Constants::J;
	_shapeGrid[2][1] = Constants::J;
}

void JShape::potentialRotate()
{
	if(_rotationCycle == 0)
	{
		clearGrid(_potentialShapeGrid);
		resize2Dimension(_potentialShapeGrid, 2, 3);

		//|L|0|0|
		//|L|L|L|
		_potentialShapeGrid[0][0] = Constants::J;
		_potentialShapeGrid[1][0] = Constants::J;
		_potentialShapeGrid[1][1] = Constants::J;
		_potentialShapeGrid[1][2] = Constants::J;
	}
	else if(_rotationCycle == 1)
	{
		clearGrid(_potentialShapeGrid);
		resize2Dimension(_potentialShapeGrid, 3, 2);
		//|L|L|
		//|L|0|
		//|L|0|
		_potentialShapeGrid[0][0] = Constants::J;
		_potentialShapeGrid[0][1] = Constants::J;
		_potentialShapeGrid[1][0] = Constants::J;
		_potentialShapeGrid[2][0] = Constants::J;
	}
	else if(_rotationCycle == 2)
	{
		clearGrid(_potentialShapeGrid);
		resize2Dimension(_potentialShapeGrid, 2, 3);
		//|L|L|L|
		//|0|0|L|
		_potentialShapeGrid[0][0] = Constants::J;
		_potentialShapeGrid[0][1] = Constants::J;
		_potentialShapeGrid[0][2] = Constants::J;
		_potentialShapeGrid[1][2] = Constants::J;
	}
	else if (_rotationCycle == 3)
	{
		clearGrid(_potentialShapeGrid);
		resize2Dimension(_potentialShapeGrid, 3, 2);
		//|0|L|
		//|0|L|
		//|L|L|
		_potentialShapeGrid[0][1] = Constants::J;
		_potentialShapeGrid[1][1] = Constants::J;
		_potentialShapeGrid[2][0] = Constants::J;
		_potentialShapeGrid[2][1] = Constants::J;
	}

	if(_rotationCycle++ == 3)
		_rotationCycle = 0;
}

void JShape::rotate()
{//check grid size(resize not done on shapeGrid)
	_shapeGrid = _potentialShapeGrid;
}

JShape::~JShape()
{

}
