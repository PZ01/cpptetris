#include "CreditsState.h"
#include "Constants.h"
#include "RM.h"
#include "Clickable.h"
#include <iostream>

const std::string CreditsState::CREDITS_TEXT1 = "Programmed by : \n\n\tPatrick Zielinski";
const std::string CreditsState::CREDITS_TEXT2 = "Art by : \n\n\tPatrick Zielinski";
const std::string CreditsState::CREDITS_TEXT3 = "BG Music by : \n\n\tKoori-kun newgrounds";
const std::string CreditsState::CREDITS_TEXT4 = "FIN";

int CreditsState::_entriesCnt = 0;

CreditsState::CreditsState(int pWindowWidth, int pWindowHeight) :
                    _text(CREDITS_TEXT1, RM::getInstance()->scoreFont()),
                    _textFieldsArr(new std::string[NUM_ENTRIES]),
                    _isDone(false)
{

    _textFieldsArr[0] = CREDITS_TEXT1;
    _textFieldsArr[1] = CREDITS_TEXT2;
    _textFieldsArr[2] = CREDITS_TEXT3;
    _textFieldsArr[3] = CREDITS_TEXT4;

    _text.setPosition(pWindowWidth / 2 - (_text.getGlobalBounds().width), pWindowHeight / 2 - (_text.getGlobalBounds().height));
    _text.setColor(sf::Color(233,210,213));
	_text.setCharacterSize(Constants::FONT_SIZE);

	_menuMenuItem = new Clickable(Constants::MENU_MENU, 100, 50, 10, 10, Constants::UPTODOWN);
	_splashTimeClock.restart();
}

int CreditsState::handleEvents(sf::RenderWindow *pWindow)
{
	using namespace sf;

	Event event;

	//Handle the menu button
	while (pWindow->pollEvent(event))
	{
		if(event.type == Event::MouseButtonPressed)
		{
			int mouseX = event.mouseButton.x;
			int mouseY = event.mouseButton.y;

			if (_menuMenuItem->isInside(mouseX, mouseY))
            {
                return Constants::STATE_MENU;
            }
		}
		else if(event.type == Event::MouseMoved)
		{
			int mouseX = event.mouseMove.x;
			int mouseY = event.mouseMove.y;

			_menuMenuItem->isInside(mouseX, mouseY);
		}
	}

    //Handle timing
    if(_splashTimeClock.getElapsedTime().asSeconds() > DELAY_ENTRIES_SECONDS && !_isDone)
    {
        _entriesCnt++;

        if(_entriesCnt == 3)
        {
            _isDone = true;
            _text.setCharacterSize(Constants::FONT_SIZE * 3);
            _text.setColor(sf::Color(135,45,172));
        }

        _text.setString(_textFieldsArr[_entriesCnt]);

        _splashTimeClock.restart();
    }


	return Constants::STATE_NULL;
}

void CreditsState::logic()
{

}

void CreditsState::draw(sf::RenderWindow *pWindow)
{
    pWindow->draw(_text);
    _menuMenuItem->draw(pWindow);
}

CreditsState::~CreditsState(void)
{
    delete _menuMenuItem;
    delete [] _textFieldsArr;
}
