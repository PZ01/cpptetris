#include "GameLoop.h"
#include "SplashState.h"
#include "MenuState.h"
#include "PlayState.h"
#include "CreditsState.h"
#include "Constants.h"

static const int MAX_FPS = 50;
static const int MAX_FRAME_SKIPS = 5;
static const int FRAME_PERIOD = 1000 / MAX_FPS;

GameLoop::GameLoop() :
	_window(sf::VideoMode::getDesktopMode(), "Blockz!", sf::Style::Fullscreen),
	_currentStateID(Constants::STATE_SPLASH),
	_nextStateID(Constants::STATE_NULL)
{


	_gameState = new SplashState();
}

void GameLoop::start()
{
	long beginTime = 0;		// the time when the cycle begun
	long timeDiff = 0;		// the time it took for the cycle to execute
	int sleepTime = 0;		// ms to sleep (<0 if we're behind)
	int framesSkipped = 0;	// number of frames being skipped
	sf::Clock clock;

	while (_window.isOpen())
	{
		beginTime = clock.getElapsedTime().asMilliseconds();
		framesSkipped = 0;

		/****************************************
		Handle Events
		****************************************/
		setNextState(_gameState->handleEvents(&_window));

		/****************************************
		Update Game State
		****************************************/
		_gameState->logic();
		changeState(); //TODO, why here...

		/****************************************
		Window rendering
		****************************************/
		_window.clear();
		_gameState->draw(&_window);
		_window.display();


		timeDiff = clock.getElapsedTime().asMilliseconds() - beginTime;
		sleepTime = (int)(FRAME_PERIOD - timeDiff);

		if (sleepTime > 0)
		{
			sf::sleep(sf::milliseconds(sleepTime));
		}

		while (sleepTime < 0 && framesSkipped < MAX_FRAME_SKIPS)
		{
			/****************************************
			Window rendering
			****************************************/
			sleepTime += FRAME_PERIOD;
			framesSkipped++;
		}
	}
}

void GameLoop::changeState()
{
	if(_nextStateID != Constants::STATE_NULL)
	{
		//Delete the current state
		if( _nextStateID != Constants::STATE_EXIT )
		{
			delete _gameState;
			_gameState = NULL;
		}
		else
		{
			_window.close();
		}

		switch( _nextStateID )
		{
		case Constants::STATE_SPLASH:
			_gameState = new SplashState();
			break;

		case Constants::STATE_MENU:
			_gameState = new MenuState(_window.getSize().x, _window.getSize().y);
			break;

		case Constants::STATE_GAME:
			_gameState = new PlayState(_window.getSize().x, _window.getSize().y);
			break;

        case Constants::STATE_CREDITS:
            _gameState = new CreditsState(_window.getSize().x, _window.getSize().y);
            break;
		}

		_currentStateID = _nextStateID;
		_nextStateID = Constants::STATE_NULL;
	}
}

void GameLoop::setNextState( int newState )
{
	if (_nextStateID != Constants::STATE_EXIT)
	{
		_nextStateID = newState;
	}
}
GameLoop::~GameLoop(void)
{
}
