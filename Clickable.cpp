#include "Clickable.h"

Clickable::Clickable(Constants::Buttons buttonID, int pWidth, int pHeight, int posX, int posY, Constants::TextureType type) :
	_xCorner(0),
	_yCorner(0),
	_type(type),
	_width(pWidth),
	_height(pHeight),
	_posX(posX), 
	_posY(posY)
{
	_texture = RM::getInstance()->specificButtonRessource(buttonID);
}

bool Clickable::isInside(int mousePosX, int mousePosY)
{
	if(mousePosX > _posX && mousePosX < (_posX + _width) &&
		mousePosY > _posY && mousePosY < (_posY + _height))
	{
		this->hover();
		return true;
	}

	this->unhover();

	return false;
}

void Clickable::hover()
{
	switch (_type)
	{
	case Constants::UPTODOWN:
		_yCorner = _height;
		_hovering = true;
	break;

	default:
		break;
	}
}

void Clickable::unhover()
{
	if(_hovering)
	{
		switch (_type)
		{
		case Constants::UPTODOWN:
			_yCorner = 0;
			break;

		default:
			break;
		}
	}
}

void Clickable::draw(sf::RenderWindow *pWindow)
{
	sf::Sprite sprite(_texture, sf::IntRect(_xCorner, _yCorner, _width, _height));
	sprite.setPosition(float(_posX), float(_posY));
	pWindow->draw(sprite);
}

Clickable::~Clickable(void)
{
}
