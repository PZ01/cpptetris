/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/
#pragma once
#include "RM.h"
#include <SFML/Graphics.hpp>

/**
A class representing a clickable entity. Two states implemented, regular and hover.
This class could use a lot of improvement.
**/
class Clickable
{
public:
	Clickable(Constants::Buttons buttonID, int pWidth, int pHeight, int posX, int posY, Constants::TextureType);
	bool isInside(int mousePosX, int mousePosY);
	void hover();
	void unhover();
	void draw(sf::RenderWindow *);

	~Clickable(void);

private:
	sf::Texture _texture;
	Constants::TextureType _type;
	bool _hovering;
	int _xCorner;
	int _yCorner;
	int _width;
	int _height;
	int _posX;
	int _posY;
};

