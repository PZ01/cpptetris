/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/

#pragma once
#include <SFML/Graphics.hpp>
#include "Constants.h"

class Clickable;

/**
This class simply represents a group of clickable entities to relieve the MenuState from having all of
the responsabilities.
**/
class Menu
{
public:
	Menu(void);
	~Menu(void);
	void draw(sf::RenderWindow *);
	Constants::GameStates clickedMenuItem(int mouseX, int mouseY);

private:
	Clickable *_startMenuItem;
	Clickable *_creditsMenuItem;
	Clickable *_quitMenuItem;
};

