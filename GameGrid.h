/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/

#pragma once
#include "Constants.h"
#include <SFML/Graphics.hpp>
#include <vector>
class Shape;

/**
This class represents the grid of blocks and holds integer positions for persistence between
drawing cycles and collision detection. The class is responsible for attempting block movements.
**/
class GameGrid
{
public:
	GameGrid(int pCols, int pRows, int pWindowWidth, int pWindowHeight);
	~GameGrid(void);
	void draw(sf::RenderWindow *);

	float getTopOfGridPosition() { return _topOfGridPosition; }
	float getLeftOfGridPosition() { return _leftOfGridPosition; }

	//TODO, might be able to use the same code in one method
	bool attemptPeriodicShapeMoveDown(Shape *pShape);
	bool attempteMoveShapeByKeys(Shape *pShape, Constants::PossibleMove);
	bool attemptRotateShape(Shape *pShape);
	void attemptInstantDrop(Shape *pShape);
	bool possiblyClearRows(int &rowsCleared);
	bool checkForLoss();

private:
	std::vector< std::vector<int> > _grid;
	sf::Sprite _board;
	sf::Sprite _bg;

	//Used for centering the grid relative to the window size
	float _topOfGridPosition;
	float _leftOfGridPosition;

	sf::Sprite colorBlockByEnum(Constants::GameShapes,int blockPosX,int blockPosY);
	void drawBlocks(sf::RenderWindow *);
	void copyShapeToGrid(Shape *pShape, const int pBlockTopRow, const int pBlockTopCol);
	std::vector<int> getFilledRows();
	bool rowIsFilled(int pRow);
	void copyLinesFromAbove(int pRowDestination);
	//Debug
	void printGrid();
};
