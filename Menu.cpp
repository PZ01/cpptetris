#include "Menu.h"
#include "Constants.h"
#include "Clickable.h"

Menu::Menu(void)
{
	_startMenuItem   = new Clickable(Constants::MENU_START,200,100,30, 30,Constants::UPTODOWN);
	_creditsMenuItem = new Clickable(Constants::MENU_CREDITS,200,100,30, 140,Constants::UPTODOWN);
	_quitMenuItem    = new Clickable(Constants::MENU_QUIT,200,100,30, 250,Constants::UPTODOWN);
}

void Menu::draw(sf::RenderWindow *pWindow)
{
	_startMenuItem->draw(pWindow);
	_creditsMenuItem->draw(pWindow);
	_quitMenuItem->draw(pWindow);
}

Constants::GameStates Menu::clickedMenuItem(int mouseX, int mouseY)
{
	if (_startMenuItem->isInside(mouseX,mouseY))
	{
		return Constants::STATE_GAME;
	}
	else if (_creditsMenuItem->isInside(mouseX,mouseY))
	{
		return Constants::STATE_CREDITS;
	}
	else if (_quitMenuItem->isInside(mouseX,mouseY))
	{
		return Constants::STATE_EXIT;
	}

}


Menu::~Menu(void)
{
    delete _startMenuItem;
    delete _creditsMenuItem;
    delete _quitMenuItem;
}
