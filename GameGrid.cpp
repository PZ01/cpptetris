#include "GameGrid.h"
#include "Shape.h"
#include "RM.h"
#include <iostream>
#include <iterator>

GameGrid::GameGrid(int pCols, int pRows, int pWindowWidth, int pWindowHeight) :
								_grid(pRows, std::vector<int>(pCols)),
								_board(RM::getInstance()->boardTexture()),
								_bg(RM::getInstance()->playStateBG())
{
	_leftOfGridPosition = (float(pWindowWidth) / 2) - ((_board.getGlobalBounds().width) / 2);
	_topOfGridPosition = (float(pWindowHeight) / 2) - ((_board.getGlobalBounds().height) / 2);
}

bool GameGrid::attemptPeriodicShapeMoveDown(Shape *pShape)
{
	unsigned int blockHeight = pShape->getShapeHeight();
	unsigned blockWidth = pShape->getShapeWidth();
	//Attempt at pushing the block down
	pShape->potentialMove(Constants::MOVE_DOWN);

	for (unsigned int row = 0; row < blockHeight; row++)
	{
		for (unsigned int col = 0; col < blockWidth; col++)
		{
			if(pShape->getBlockAt(row, col) != 0)
			{
				unsigned int trueRow = row + pShape->getPotentialY();
				unsigned int trueCol = col + pShape->getPotentialX();
				int collisionRow = pShape->getRow();
				int collisionCol = pShape->getCol();

				if (trueRow >= _grid.size())
				{
					sf::Clock c;
					c.restart();
					copyShapeToGrid(pShape, collisionRow, collisionCol);
					pShape->potentialMove(Constants::MOVE_UP);

					return false;
				}

				if (_grid.at(trueRow).at(trueCol) != 0)
				{
					copyShapeToGrid(pShape, collisionRow, collisionCol);
					pShape->potentialMove(Constants::MOVE_UP);
					return false;
				}
			}
		}
	}

	//If we got here, the block has not encountered any collision
	//we change it's position.
	pShape->move(Constants::MOVE_DOWN);
	return true;
}

bool GameGrid::attempteMoveShapeByKeys(Shape *pShape, Constants::PossibleMove pMove)
{
	unsigned int blockHeight = pShape->getShapeHeight();
	unsigned int blockWidth = pShape->getShapeWidth();
	pShape->potentialMove(pMove);

	for (unsigned int row = 0; row < blockHeight; row++)
	{
		for (unsigned int col = 0; col < blockWidth; col++)
		{
			if(pShape->getBlockAt(row, col) != 0)
			{
				unsigned int trueRow = row + pShape->getPotentialY();
				unsigned int trueCol = col + pShape->getPotentialX();

				if(trueCol < 0 || trueCol >= Constants::GRID_WIDTH || trueRow >= _grid.size())
				{
					//Potential move is out of bounds, we must reverse it.
					if(pMove == Constants::MOVE_DOWN)
					{
						pShape->potentialMove(Constants::MOVE_UP);
						return false;
					}
					pShape->potentialMove((Constants::PossibleMove)(pMove * -1));
					return false;
				}

				if (_grid.at(trueRow).at(trueCol) != 0)
				{
					if(pMove == Constants::MOVE_DOWN)
					{
						pShape->potentialMove(Constants::MOVE_UP);
						return false;
					}
					pShape->potentialMove((Constants::PossibleMove)(pMove * -1));
					return false;
				}
			}
		}
	}

	pShape->move(pMove);
	return true;
}

bool GameGrid::attemptRotateShape(Shape *pShape)
{
	//If were on the far edge from either 8 or 9 we can't move.
	if (pShape->getCol() == (Constants::GRID_WIDTH - 1) || pShape->getCol() == (Constants::GRID_WIDTH - 2))
		return false;

	unsigned int blockHeight = pShape->getShapeHeight();
	unsigned int blockWidth = pShape->getShapeWidth();
	pShape->potentialRotate();

	for (unsigned int row = 0; row < blockHeight; row++)
	{
		for (unsigned int col = 0; col < blockWidth; col++)
		{
			unsigned int trueRow = row + pShape->getPotentialY();
			unsigned int trueCol = col + pShape->getPotentialX();

			if(trueCol < 0 || trueCol >= Constants::GRID_WIDTH || trueRow >= _grid.size())
			{
				pShape->decreaseRotationCycle();
				return false;
			}

			if (_grid.at(trueRow).at(trueCol) != 0)
			{
				pShape->decreaseRotationCycle();
				return false;
			}
		}
	}

	pShape->rotate();
	return true;
}

void GameGrid::attemptInstantDrop(Shape *pShape)
{
    //Should be changed to look straight at the bottom...
	while (attemptPeriodicShapeMoveDown(pShape)) {}
}

sf::Sprite GameGrid::colorBlockByEnum(Constants::GameShapes pGameShape, int pRow, int pCol)
{
	//WARNING, it is the responsability of the asset provider to
	//create a board with which is divisible by the size of the grid.
	int blockWidth = (RM::getInstance()->boardTexture().getSize().x - Constants::GRID_BORDER_WIDTH) / Constants::GRID_WIDTH;
	int blockHeight = RM::getInstance()->blocksTexture().getSize().y;
	//Must substract by one, shapes index starts at 1, see Constants.
	int blockIdx = blockWidth * (pGameShape  - 1);

	//WARNING, this current implementation assumes the asset is horizontal
	sf::IntRect blockMask(blockIdx, 0, blockWidth, blockHeight);
	sf::Sprite blockSprite(RM::getInstance()->blocksTexture(), blockMask);

	blockSprite.setPosition(float(pCol * blockWidth)  + _leftOfGridPosition,
							float(pRow * blockHeight) + _topOfGridPosition);

	return blockSprite;
}

bool GameGrid::checkForLoss()
{
	bool blockAtTop = false;
	for (int col = 0 ; col < Constants::GRID_WIDTH; ++col)
		if(_grid[0][col] != 0)
			blockAtTop = true;

	return blockAtTop;
}

bool GameGrid::possiblyClearRows(int &rowsCleared)
{
	bool rowsHaveBeenClered = false;
	int row = 0;
	for (; row < Constants::GRID_HEIGHT; row++)
	{
		if (rowIsFilled(row))
		{
			copyLinesFromAbove(row);
			rowsHaveBeenClered = true;
		}
	}

    rowsCleared = row;
	return rowsHaveBeenClered;
}

void GameGrid::copyLinesFromAbove(int pRowDestination)
{
	for (pRowDestination ; pRowDestination > 0; --pRowDestination)
		_grid[pRowDestination] = _grid[pRowDestination - 1];

	for (int col = 0 ; col < Constants::GRID_WIDTH; ++col)
		_grid[pRowDestination][col] = 0;
}

void GameGrid::copyShapeToGrid(Shape *pShape, const int pBlockTopRow, const int pBlockTopCol)
{
	unsigned int blockHeight = pShape->getShapeHeight();
	unsigned int blockWidth = pShape->getShapeWidth();

	for (unsigned int row = 0; row < blockHeight; row++)
	{
		for (unsigned int col = 0; col < blockWidth; col++)
		{
			if(pShape->getBlockAt(row, col) != 0)
			{
				int trueRow = pBlockTopRow + row;
				int trueCol = pBlockTopCol + col;
				_grid.at(trueRow).at(trueCol) = pShape->getShapeType();
			}
		}
	}
}

bool GameGrid::rowIsFilled(int pRow)
{
	for (unsigned int col = 0; col < Constants::GRID_WIDTH; col++)
	{
		if (_grid[pRow][col] == 0)
		{
			return false;
		}
	}

	return true;
}

void GameGrid::draw(sf::RenderWindow *pWindow)
{
	//Draw background
	pWindow->draw(_bg);

	//Draw board over background
	_board.setPosition((_leftOfGridPosition - Constants::GRID_BORDER_WIDTH/2), _topOfGridPosition);
	pWindow->draw(_board);

	//Draw blocks over board
	drawBlocks(pWindow);
}

void GameGrid::drawBlocks(sf::RenderWindow *pWindow)
{
	for (unsigned int row = 0; row < _grid.size(); row++)
	{
		for (unsigned int col = 0; col < _grid[row].size(); col++)
		{
			if(_grid[row][col] != 0)
			{
				Constants::GameShapes shapeID = (Constants::GameShapes) (_grid[row][col]);
				pWindow->draw(colorBlockByEnum(shapeID, row , col));
			}
		}
	}
}

void GameGrid::printGrid()
{
	using namespace std;
	for(unsigned int i = 0 ; i < _grid.size() ; i++)
	{
		copy(_grid[i].begin(), _grid[i].end(), ostream_iterator<int>(std::cout, " "));
		cout << "\n";
	}
	cout << endl << "------------------"<< endl;
}

GameGrid::~GameGrid(void)
{
}
