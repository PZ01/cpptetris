/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/

#pragma once
#include "shape.h"

class LShape : public Shape
{
public:
	LShape(Constants::GameShapes pShapeType, int pPosX);
	~LShape(void);

private:
	void rotate();
	void potentialRotate();
};

