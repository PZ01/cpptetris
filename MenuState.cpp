#include "MenuState.h"
#include "Constants.h"
#include <iostream>
#include "RM.h"

MenuState::MenuState(int pWindowWidth, int pWindowHeight) : _menu(), _menuBG(RM::getInstance()->menuBG())
{
	RM::getInstance()->bgMusic().play();
	_menuBG.setPosition(Constants::BG_MENU_POSX,Constants::BG_MENU_POSY);
}

int MenuState::handleEvents(sf::RenderWindow *pWindow)
{
	sf::Event event;
	while (pWindow->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			pWindow->close();
		else if(event.type == sf::Event::MouseButtonPressed)
		{
			int mouseX = event.mouseButton.x;
			int mouseY = event.mouseButton.y;

			return _menu.clickedMenuItem(mouseX,mouseY);
		}
		else if(event.type == sf::Event::MouseMoved)
		{
			int mouseX = event.mouseMove.x;
			int mouseY = event.mouseMove.y;

			_menu.clickedMenuItem(mouseX,mouseY);
		}

	}

	return Constants::STATE_NULL;
}

void MenuState::logic()
{

}

void MenuState::draw(sf::RenderWindow *pWindow)
{
	pWindow->draw(_menuBG);
	_menu.draw(pWindow);
}

MenuState::~MenuState(void)
{
}
