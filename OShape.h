/*============================================================================
Author : Patrick Zielinski
This code may be copied or modified for EDUCATIONAL purposes only.
This code may not be copied for commercial intent.

Source written in 2013
============================================================================*/

#pragma once
#include "Shape.h"

class OShape : public Shape
{
public:
	OShape(Constants::GameShapes pShapeType, int pPosX);
	~OShape(void);
};

